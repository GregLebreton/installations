#!/bin/bash

sudo apt update -y
sudo apt upgrade -y

sudo service ssh start

sudo apt install openjdk-11-jdk

wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
sudo sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/ > \
    /etc/apt/sources.list.d/jenkins.list'
sudo apt-get update
sudo apt-get install jenkins -y

sudo cat /var/lib/jenkins/secrets/initialAdminPassword

# Jenkins : http://IP-ADRESS:8080
