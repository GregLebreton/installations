#!/bin/bash

installamd64 ()
{
    sudo apt-get install apt-transport-https ca-certificates curl gnupg -y
    curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
    echo \
    "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] \
    https://download.docker.com/linux/debian \
    $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    sudo apt update -y
    sudo apt-get install docker-ce docker-ce-cli containerd.io -y
    dockerSudo
}

installarm64 ()
{
    apt-get install apt-transport-https ca-certificates curl gnupg -y
    curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
    echo \
    "deb [arch=arm64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] \
    https://download.docker.com/linux/debian \
    $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    apt update -y
    apt-get install docker-ce docker-ce-cli containerd.io -y
    dockerSudo
}

dockerSudo ()
{
    sudo groupadd docker
    sudo usermod -aG docker $USER
    newgrp docker
}

# START
# CHECK LINUX ARCH
arch=$(uname -m)
if [ $arch == "x86_64" ]
then
{
    installamd64
}
elif [ $arch == "aarch64" ]
then
{
    installarm64
}
fi
